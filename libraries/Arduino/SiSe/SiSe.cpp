#include <Arduino.h>
#include <Adafruit_GFX.h>    // Core graphics library
#include <Adafruit_ST7735.h> // Hardware-specific library
#include <SiSe.h>

Adafruit_ST7735 tft = Adafruit_ST7735(TFT_CS,  TFT_DC, TFT_RST);

uint16_t RGB( uint8_t R, uint8_t G, uint8_t B)
{
  return ( ((R & 0xF8) << 8) | ((G & 0xFC) << 3) | (B >> 3) );
}
textBox::textBox(uint8_t x, uint8_t y, uint16_t col){
posX = x;
posY = y;
textBox_height = TEXT_BOX_HEIGHT;
textBox_width = TEXT_BOX_WIDTH;
color = col;
isPassword = false;
tex_color = col;
focus = false ;
on_blink = 800;
off_blink = 300;
previousMillis = 0;
blink_state = false;
text_flag = 0;
currentCharPosX = posX + 4;
bkg_color = ST7735_BLACK;
run =true;
focus_color = RGB(DEFAULT_FOCUS_COLOR);
}
void textBox::draw() {
    tft.drawRect(posX,posY ,textBox_width, textBox_height, color);	
}
void textBox::clear_box(){
	setBackground();
	for (int i = 0; i < MAX_INPUT; i++){
		myText[i] = '\0';
	}
}
void textBox::setPassBox(boolean pass){
isPassword = pass;
}
void textBox::setTextColor(uint16_t newTextColor){
tex_color = newTextColor;
}
void textBox::setColor(uint16_t newColor){
color = newColor;
}
void textBox::setFocusColor(uint16_t focusColor){
	focus_color = focusColor;
}
void textBox::setFocus(boolean focused){
	focus = focused;	
	if (focus){
		fillBox(posX+2, posY+2, textBox_width-4, textBox_height-4, focus_color);
		//titila();
	}
	else {
		setBackground();
		if(isPassword){
			char texto_pass[11];
			int i = 0;
			while(myText[i]){
				texto_pass[i] = 'x';
				i++;
			}
			texto_pass[i] = '\0';
				putText(texto_pass, posX + 4, posY + 4,tex_color);
		}
		else 
			putText(myText,posX + 4, posY + 4,tex_color);
	} 
}
boolean textBox::isFocused(){
	return focus;
}
void textBox::titila(){	
if (text_flag)
	currentCharPosX+= CHARACTER_STEP;  		
if (currentCharPosX >10) currentCharPosX = 10;
	uint16_t currentMillis = millis();	
	if ((blink_state) && currentMillis - previousMillis >= on_blink){			
		blink_state = !blink_state;
		putChar('_',currentCharPosX, posY + Y_PADDING_TEXT,tex_color);
		previousMillis = currentMillis;
	}
	else if((!blink_state)&& currentMillis - previousMillis >= off_blink){
		blink_state = !blink_state;
		putChar(' ',currentCharPosX,posY + Y_PADDING_TEXT,tex_color);
	    previousMillis = currentMillis;
	}	
}

void textBox::setText (char testo)
{ 
	if (text_flag < MAX_INPUT){
		if (run){			
			if (isPassword){				
			    if (!text_flag){				
					putChar('x', currentCharPosX, posY + Y_PADDING_TEXT, tex_color);
					currentCharPosX = currentCharPosX - CHARACTER_STEP;
				}
				else 
					putChar('x', currentCharPosX + CHARACTER_STEP, posY + Y_PADDING_TEXT, tex_color);
					currentCharPosX = currentCharPosX + CHARACTER_STEP;
					myText[text_flag] = testo;
					text_flag++;
				if (!testo)	{
					run = false;
					setFocus(false);	
				}					
			}
			else{ 
				if (!text_flag){				
					putChar(testo, currentCharPosX, posY + Y_PADDING_TEXT, tex_color);
					currentCharPosX = currentCharPosX - CHARACTER_STEP;
				}
				else 
					putChar(testo, currentCharPosX + CHARACTER_STEP, posY + Y_PADDING_TEXT, tex_color);
				currentCharPosX = currentCharPosX + CHARACTER_STEP;
				myText[text_flag] = testo;
				text_flag++;
				if (!testo)	{
					run = false;
					setFocus(false);	
				}
			}
		}
	}
}
char* textBox::getText(){
	return myText;
}
void textBox::setBackground(){
fillBox(posX+2, posY+2, textBox_width-4, textBox_height-4, bkg_color);	
}
void textBox::setBackgroundColor (uint16_t b_color){
bkg_color = b_color;	 
}

void clear_lcd(){
 tft.fillScreen(DEFAULT_CLEAR_COLOR);	
}
msgBox::msgBox(){
	posX = 60;
	posY = 80;
	isVisible = false;
	run = true;
}
void msgBox::hide(){
		tft.fillRoundRect(posX-20, posY , 80, 30, 8, DEFAULT_CLEAR_COLOR);
}
boolean msgBox::isVisibled(){	
	return isVisible;
}
void msgBox::setVisibled(boolean vista){
	isVisible = vista;
}
void msgBox::show(uint8_t tipo){
		if (isVisible){ 
			switch (tipo){
				case MSG_ACCESS :
				tft.fillRoundRect(posX, posY , 40, 30, 8, RGB(124,252,0));
				putText("ACCESO", posX + 2 , posY + (30/2) - 4, ST7735_BLACK);			
				break;
				case MSG_WRONG :
				tft.fillRoundRect(posX-20, posY , 80, 30, 8, 0x001F);
				putText("NO PERMITIDO", posX + 2 -18 , posY + (30/2) - 4, ST7735_BLACK);
				break;
				case MSG_INFO:
				break;
			}
			delay(1500);
			hide();
			isVisible = false;
		}	
}

void putText(char *text,uint8_t x,uint8_t y, uint16_t color) {
  tft.setCursor(x, y);
  tft.setTextColor(color);
  tft.setTextWrap(false);
  tft.print(text);
}
void putChar(char text,uint8_t x,uint8_t y, uint16_t color) {
  tft.setCursor(x, y);
  tft.setTextColor(color);
  tft.setTextWrap(false);
  tft.print(text);
}

void fillBox(uint8_t x, uint8_t y, uint8_t width, uint8_t height, uint16_t color) {
    tft.fillRect(x, y, width, height, color);
}

void initlcd(){
  tft.initR(INITR_GREENTAB);   // initialize a ST7735SR chip, green tab
  tft.fillScreen(ST7735_BLACK);
  tft.setRotation(1); //landscape  
}
void Button() {
  // play
  tft.fillRoundRect(50, 50, 78, 60, 8, ST7735_WHITE);

  //  tft.fillTriangle(42, 20, 42, 60, 90, 40, ST7735_GREEN);
}


