#ifndef SiSe_h
#define SiSe_h

#include <Arduino.h>
#include <Adafruit_GFX.h>    // Core graphics library
#include <Adafruit_ST7735.h> // Hardware-specific library
#include <SPI.h>

#define TFT_CS     10
#define TFT_DC     9 
#define TFT_RST    8
#define CHARACTER_STEP 6
#define MAX_INPUT 12
#define Y_PADDING_TEXT 4
#define TEXT_BOX_HEIGHT 15
#define TEXT_BOX_WIDTH 80
#define DEFAULT_FOCUS_COLOR 112,128,144 
#define MSG_ACCESS 0
#define MSG_WRONG 1
#define MSG_INFO 2 
#define DEFAULT_CLEAR_COLOR ST7735_BLACK
void clear_lcd();
uint16_t RGB( uint8_t R, uint8_t G, uint8_t B);
void putChar(char text, uint8_t x,uint8_t y, uint16_t color);
//--------------------------
class msgBox {
private:
uint8_t posX;
uint8_t posY;
char*	msg;
uint8_t text_color;
boolean isVisible;
boolean run;
public :
msgBox();
void show(uint8_t tipo);
void hide ();
boolean isVisibled();	
void setVisibled(boolean vista);
};
class textBox
{	
uint8_t posX;
uint8_t posY;
uint8_t textBox_height;
uint8_t textBox_width;
uint16_t color;
uint16_t tex_color;
uint16_t on_blink;
uint16_t off_blink;
boolean blink_state;
uint16_t previousMillis;
boolean isPassword;
boolean focus;
char myText[MAX_INPUT];
uint8_t text_flag;
uint8_t currentCharPosX;
uint16_t bkg_color;
uint16_t focus_color;
boolean run;

public :
void clear_box();
textBox(uint8_t x, uint8_t y, uint16_t col);
void draw();
void titila();
void setFocus(boolean focused);
void setPassBox(boolean pass);
void setTextColor(uint16_t newTextColor);
void setColor(uint16_t newColor);
void setText (char testo);
void setBackgroundColor (uint16_t b_color);
void setBackground();
void setFocusColor(uint16_t);
char* getText(); 
boolean isFocused();
};
void putText(char *text,uint8_t x,uint8_t y, uint16_t color);
void fillBox(uint8_t x, uint8_t y, uint8_t width, uint8_t height, uint16_t color);
void initlcd();
void loadSiSe();
#endif