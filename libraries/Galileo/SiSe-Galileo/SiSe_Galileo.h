#ifndef SiSe_Galileo_h
#define SiSe_Galileo_h
#include<Arduino.h>
// LCD PINS 
#define RS 11    
#define EN 9   
#define D4 6
#define D5 5
#define D6 3
#define D7 2
#define MAX_INPUT 10
#define CHARACTER_STEP   1
#define USER false
#define PASS true 

void clear_lcd();
void putChar(char text, uint8_t x, uint8_t y);
void putText(char *text, uint8_t x, uint8_t y);

//--------------------------
class textBox
{
boolean blink_state;	
boolean isPassword;
boolean focus;
boolean run;
uint16_t previousMillis;
uint8_t text_flag;
uint8_t currentCharPosX;
char myText[MAX_INPUT];

public :
void clear_box();
textBox(boolean tipo);
void draw();
void titila();
void setFocus(boolean focused);
void setText (char testo);
char* getText(); 
boolean isFocused();
};
#endif