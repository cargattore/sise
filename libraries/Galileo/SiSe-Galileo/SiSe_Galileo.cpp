#include <Arduino.h>
#include<LiquidCrystal.h>
#include <BitBool.h>
#include <OnewireKeypad.h>
#include <SiSe_Galileo.h>

char KEYPAD [] = {
	'7','8','9','A',
	'4','5','6','B',
	'1','2','3','C',
	'*','0','\0','D'
};

LiquidCrystal lcd = LiquidCrystal(RS,EN,D4,D5,D6,D7);
OnewireKeypad <Print, 16 > KP(lcd, KEYPAD, 4, 4, A0, 4700, 1000, ExtremePrec );

//----------------------
char key_pressed(){
  char lera;
  byte KS = KP.Key_State();
    if (KS == PRESSED) {
      if ( lera = KP.Getkey() ) {
      return lera;
     }
   }
}
textBox::textBox(boolean tipo){
isPassword = tipo;
focus = false ;
previousMillis = 0;
blink_state = false;
text_flag = 0;
currentCharPosX = 6;
run =true;
}
void textBox::draw() {
	if(!isPassword){
	lcd.setCursor(0,1);
	lcd.print("user:");
	}
    else {
	lcd.setCursor(0,2);
    lcd.print("pass:");	
	}
}
void textBox::clear_box(){
	
	for (int i = 0; i < MAX_INPUT; i++){
		myText[i] = '\0';//relleno de NULL
	}
}
void textBox::setFocus(boolean focused){
	focus = focused;	
	if (focus){
		
		//titila();
	}
	else {
		if(isPassword){
			char texto_pass[11];
			int i = 0;
			while(myText[i]){
				texto_pass[i] = 'x';
				i++;
			}
			texto_pass[i] = '\0';
				putText(texto_pass, 4,4);
		}
		else 
			putText(myText,4, 4);
	} 
}
boolean textBox::isFocused(){
	return focus;
}
void textBox::titila(){	
if (text_flag)
	currentCharPosX+= CHARACTER_STEP;  		
if (currentCharPosX >10) currentCharPosX = 10;
	unsigned long currentMillis = millis();	
	if ((blink_state) && currentMillis - previousMillis >= 300){			
		blink_state = !blink_state;
		putChar('_',currentCharPosX, 4);
		previousMillis = currentMillis;
	}
	else if((!blink_state)&& currentMillis - previousMillis >= 400){
		blink_state = !blink_state;
		putChar(' ',currentCharPosX,4 );
	    previousMillis = currentMillis;
	}	
}

void textBox::setText (char testo)
{ 
	if (text_flag < MAX_INPUT){
		if (run){			
			if (isPassword){				
			    if (!text_flag){				
					putChar('x', currentCharPosX, 1 );
					currentCharPosX = currentCharPosX - CHARACTER_STEP;
				}
				else 
					putChar('x', currentCharPosX + CHARACTER_STEP, 1);
					currentCharPosX = currentCharPosX + CHARACTER_STEP;
					myText[text_flag] = testo;
					text_flag++;
				if (!testo)	{
					run = false;
					setFocus(false);	
				}					
			}
			else{ 
				if (!text_flag){				
					putChar(testo, currentCharPosX, 0);
					currentCharPosX = currentCharPosX - CHARACTER_STEP;
				}
				else 
					putChar(testo, currentCharPosX + CHARACTER_STEP, 0);
				currentCharPosX = currentCharPosX + CHARACTER_STEP;
				myText[text_flag] = testo;
				text_flag++;
				if (!testo)	{
					run = false;
					setFocus(false);	
				}
			}
		}
	}
}
char* textBox::getText(){
	return myText;
}

void clear_lcd(){
 lcd.clear();	
}

void putText(char *text, uint8_t x, uint8_t y) {
  
}
void putChar(char text, uint8_t x, uint8_t y) {

}


