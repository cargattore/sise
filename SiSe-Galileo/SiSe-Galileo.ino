#include <SiSe_Galileo.h>
textBox user(USER);
textBox pass(PASS);
void setup() {
user.draw();
pass.draw();  
user.setFocus(true);
}
void loop(){
if (user.isFocused()){
  user.run();
  pass.setFocus(true);
  }
 if (pass.isFocused()){
  pass.run();
  user.clear_box();
  pass.clear_box();
  user.setFocus(true);
  } 
}
