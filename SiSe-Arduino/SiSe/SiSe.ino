#include <Time.h>
#include <TimeLib.h>
#include <Adafruit_GFX.h>    // Core graphics library
#include <Adafruit_ST7735.h> // Hardware-specific library
#include <SPI.h>
#include <SD.h>
#include <stdio.h>
#include <string.h>
#include <SiSe.h>
void dataLogger(boolean permitido);
textBox user_tb = textBox(40,20,ST7735_WHITE);
textBox pass_tb = textBox(40,42,ST7735_WHITE);
msgBox msg = msgBox();
File myFile;
String db = "";
char incoming = 0;
int i = 0;
void setup(void) {
  // Use this initializer if you're using a 1.8" TFT
  currentTime();
  initlcd();
  loadSiSe();
  //user_tb.isFocused(true);
  //text_box2.setText("************");
//  tft.drawPixel(tft.width()/2, tft.height()/2, ST7735_GREEN);
   Serial.begin(115200);
   Serial.print("Initializing SD card...");
  if (!SD.begin(4)) {
    Serial.println("initialization failed!");
    return;
  }
  Serial.println("initialization done.");
  myFile = SD.open("user_db");
  if (myFile) {
    Serial.println("DB:");
    // read from the file until there's nothing else in it:
    while (myFile.available()) {
      incoming = myFile.read();
      Serial.write(incoming);
      db = db + incoming; 
    }
    // close the file:
    myFile.close();
  } else {
    // if the file didn't open, print an error:
    Serial.println("error opening test.txt");
  } 
  Serial.println("db:");
  Serial.println(db);
   user_tb.setFocus(true);
    
}
void loop() {
  
 if (user_tb.isFocused()){ 
 user_tb.setText('j');
  delay(1000);
 user_tb.setText('l');
  delay(1000);
 user_tb.setText('e');
  delay(1000);
 user_tb.setText('i');
  delay(1000);
 user_tb.setText('v');
  delay(1000);
  user_tb.setText('a');
  delay(1000);
 user_tb.setText('\0');
  delay(1000); 
 pass_tb.setPassBox(true);
 pass_tb.setFocus(true);
 }
 if (pass_tb.isFocused()){
   pass_tb.setText('p');
  delay(1000);
 pass_tb.setText('a');
  delay(1000);
 pass_tb.setText('s');
  delay(1000);
  pass_tb.setText('s');
  delay(1000); 
pass_tb.setText('w');
  delay(1000);
pass_tb.setText('o');
  delay(1000);
pass_tb.setText('r');
  delay(1000);
pass_tb.setText('d');
  delay(1000);
pass_tb.setText('\0');
  delay(1000); 
 msg.setVisibled(true);
 }
 if (msg.isVisibled()){ 
    char myQuery[26];
    sprintf(myQuery,"%s,%s",user_tb.getText(), pass_tb.getText());
    Serial.println(myQuery);    
     if (db.indexOf(myQuery)>= 0){
      msg.show(MSG_ACCESS);
      dataLogger(true);
      }
      else{ 
      msg.show(MSG_WRONG);
      dataLogger(false);
      }
     user_tb.clear_box();
     pass_tb.clear_box();
 }
}
void currentTime(){
char * horas =__TIME__;
char * minutos;
char * segundos;
int Hora, Min, Sec;
horas = strtok (horas,":"); // 45:00
minutos = strtok(NULL, ":");
segundos = strtok(NULL, ":"); 
Hora = atoi(horas);
Min = atoi(minutos);
Sec = atoi (segundos);

char * dia = __DATE__;
char * mes;
char * anio;
mes = strtok (dia, " ");
dia = strtok(NULL," ");
anio = strtok(NULL," ");
const char *monthName[12] = {
  "Jan", "Feb", "Mar", "Apr", "May", "Jun",
  "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"
};
uint8_t monthIndex;
for (monthIndex = 0; monthIndex < 12; monthIndex++) {
    if (strcmp(mes, monthName[monthIndex]) == 0) break;
  }
int Mes,Dia,Anio;
Dia = atoi(dia);
Mes = monthIndex + 1;
Anio = atoi(anio);
setTime(Hora,Min,Sec,Dia,Mes,Anio);
}
void dataLogger(boolean permitido){
//currentTime();
time_t t = now();
char dataString[100];
char date[10];
char hora[10];    
sprintf(hora,"%i:%i:%i",hour(t),minute(t),second(t));
sprintf(date,"%i/%i/%i",day(t),month(t),year(t));
if (permitido)
{
  char *pass= "pass";
  char *acs="Permitido";
  sprintf(dataString,"\"%s\",\"%s\",\"%s\",\"%s\",\"%s\"",date,hora,user_tb.getText(),pass,acs);
}else   
{
  char *acs="No Permitido";
  sprintf(dataString,"\"%s\",\"%s\",\"%s\",\"%s\",\"%s\"",date,hora,user_tb.getText(),pass_tb.getText(),acs);
}
// so you have to close this one before opening another.
  File dataFile = SD.open("data_uni.csv", FILE_WRITE);
  // if the file is available, write to it:
  if (dataFile) {
    dataFile.println(dataString);
    dataFile.close();
    // print to the serial port too:
    Serial.println(dataString);
  }
  // if the file isn't open, pop up an error:
  else {
    Serial.println("error opening datalog.txt");
  }  
}
void loadSiSe(){
  clear_lcd();
  putText("SiSe-v0.1 - Bienvenido ",0,0,ST7735_WHITE);
  putText("User",2,22,ST7735_WHITE);
  putText("Pass",2,44,ST7735_WHITE); 
  user_tb.draw();
  pass_tb.draw();		
}
